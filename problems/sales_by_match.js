/*
There is a large pile of socks that must be paired by color. 
Given an array of integers representing the color of each sock, 
determine how many pairs of socks with matching colors there are.

Example:

n = 7;
ar = [1,2,1,2,1,3,2];

There is one pair of color 1 and one of color 2. There are three odd socks left, one of each color. The number of pairs is 2.
*/

//SOLUTION 1.

let arr = [1,2,1,1,3,2];

function countPair1(arr){
  arr.sort((a, b) => a - b); //Upotreba custom sort-a jer bez toga moze biti primer [ 10, 2, 3, 50, 6 ]

  //Prvo sto radimo je sortiranje, dobicemo input [1,1,1,2,2,2,3]

  let pair = 0; // Broj parova na pocetku je 0

  for (let i = 0; i < arr.length; i+=2) { //prolazimo kroz sortiran array u parova tj povecavamo index za 2
    if(arr[i] === arr[i+1]) pair++ // Ako su prvi broj i drugi broj isti, dodajemo par
    else{ 
      i -= 1 // Ukoliko nisu, oduzimamo 1 index, i kasnije dodajemo 2 u sledecoj iteraciji
    }
  }

  return pair;
}

console.log(countPair1(arr), "SOLUTION 1.")

//SOLUTION 2.


function countPair2(arr){
  let counted = []
  let pair = 0;

  arr.forEach(number=>{
    if(!counted.includes(number)){
      counted.push(number)
      let filtered = arr.filter(element => element === number);
      pair += Math.floor(filtered.length / 2)
    }
  })

  return pair
}


let pairs = countPair2(arr)
console.log(pairs, "SOLUTION 2.")



//[1,1,1,2,2,2,3]

