/*
There is a string, S, of lowercase English letters that is repeated infinitely many times. Given an integer, N, 
find and print the number of letter a's in the first N letters of the infinite string.

Example: 

s = "aba"
n = 10

The substring we consider is 'abaabaabaa', the first  characters of the infinite string. 
There are N occurrences of a in the substring.

So if we count letter "a" we will get number 7, because there is 7 ocurance of letter "a"
*/

let w = 'aba';
let n = 10;

let c = repeatedString(w, n, 'a');

console.log(c);

function repeatedString(s, n, l = 'a') {

  /*
    U ovoj liniji koda brojimo koliko puta se odredjeno slovo ponavlja u jednom ponavljanju
    u slucaju reci "aba", ponavlja se 2x;

    Pretvaramo string u array tako sto delimo slova na "" tj na svakom slovu
    i dobijamo 

    ["a", "b", "a"]

    zatim funkcijom filter, izvlacimo samo slova "a",
    i dobijamo 
    ["a", "a"]

    i racunamo duzinu tog niz-a tj array-a i dobijamo 2
  */
  let countInSingle = s.split('').filter((e) => e === l).length; 


  /*
    Ovde racunamo koliko puta trebamo da ponovimo rec da bi smo dosli priblizno broju N, i to bez ostatka
    jer ostatak moze da varira, recimo samo 1 slovo ili 2...

    U nasem slucaju "aba" imamo ovako

    i = Math.floor(10 / 3) i dobicemo 3 umesto 3.3333333...

    ! Math.floor() funkcija koja zaokruzuje na donju vrednost Math.floor(3.9999) = 3
  */
  let i = Math.floor(n / s.length);


  /*
    S obzirom da u nekim slucajevima imamo ostatak prilikom izracunavanja iznad,
    sto automatski znaci da ima vise ponavljanja nego sto smo dobili, primer "aba",

    aba(1) aba(2) aba(3) a(0.333)

    Mi gore u varijabli i imamo samo 3, znaci da nam fali 0.333333....

    To mozemo da izracunamo na sledeci nacin

    Od zeljenog broja reci N, u ovom slucaju 10, oduzimamo i(3) * s.length(3);

    10 - 3*3 = 1

    i dobijamo broj 1,
    sto znaci da nam od cele reci fali jedno slovo

    ili tvam od "aba" nam fali samo jedno tj prvo slovo, u ovom slucaju "a"
  */
  let missingLetterCount = n - i * s.length;


  /*
    U ovom delu cemo celu rec "aba" da skratimo samo na potrebnu duzinu
    tj. da uzmemo samo deo koji nam fali, ili vam u nasem slucaju "a"

    missingLetter = "a"
  */
  let missingLetter = s.substring(0, missingLetterCount); 


  //Na isti nacin kao na pocetku racunamo koliko puta se ponavlja slovo ali 
  //ovog puta samo na missingLetter koji je u nasem slucaju samo "a"
  let additionalCount = missingLetter.split('').filter((e) => e === l).length;

  /*
   Sada kada imamo sve potrebne varijable mozemo da izracunamo 

    2 puta se ponavlja u jednoj reci
    3 puta moramo da ponovimo rec da bi smo stigli do najblize duzine reci u odnosu na N
    1 Je broj ponavljanja u ostatku

    2 * 3 + 1 = 7;
   */

  return countInSingle * i + additionalCount;
}
