function Emmiter(){
  this.events = {}; //property events ce objekat sa poljima koji su nazivi evenata i oni ce biti 
  // array funkcija koje treba da se okinu kada se odredjeni event emmit-uje

  // {
  //   "test": [Function, Function]
  // }

} // u ovom primeru koristimo function umesto class, cisto radi vezbanja :D 

Emmiter.prototype.on = function(eventName, func){
  this.events[eventName] = this.events[eventName] || [] // u slucaju da je this.events[eventName] undefined, kreirace samo prazan array;

  //Ukoliko nije postojalo polje, iznd smo kreirali, a u delu ispod mi dodajemo funkciju u taj array
  this.events[eventName].push(func)

  // tako da sada imamo ovako nesto

  // {
  //   "test": [Function, Function]
  // }
}

Emmiter.prototype.emit = function(eventName){
  if(!this.events[eventName]){ //U slucaju da event sa tim nazivom ne postoji isprintace error 
    console.log("Event with name " + eventName + " is not created") 
    return
  }


  // U ovom delu pokrecemo sve funkcije koje su povezane sa tim eventom
  // tacnije pronalazimo polje u objektu events koje ima naziv koji smo prosledili
  // prolazimo kroz sve funkcije i pozivamo ih
  for (let i = 0; i < this.events[eventName].length; i++) {
    const func = this.events[eventName][i]; //preuzeli smo funkcije sa indexom 0 i smestili je u konstanti func
    func() // pozivamo/emitujemo tu funkciju
  }
}

module.exports = Emmiter

